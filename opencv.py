# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np
from skimage import io,exposure
from skimage.color import rgb2gray
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import cv2 as cv # librairie OpenCV
from matplotlib.colors import ListedColormap
# o r d re de s s i g n i f i c a t i o n s de s c o u l e u r s : FP en rouge , FN en orange ," r i e n " en bl anc , VN en bl e u , VP en v e r t
occcmap = ListedColormap (["red" ,"orange" ,"white","blue","green"] )

#%matplotlib auto
#ouvrirles images en RGB et en niveaux degris dans [ [ 0 ; 2 5 5 ] ]
Ig = io.imread("./teddy/im2.png")
Id = io.imread("./teddy/im6.png")
gt = io.imread("./teddy/disp2.png")
Iggray = (rgb2gray(Ig)*255).astype(np.uint8)
Idgray = (rgb2gray(Id)*255).astype(np.uint8)
fig,axes = plt.subplots(2,2,sharex=True,sharey=True)
# attention avec ces implementations ,
# numDisparities doit etre divisible par 16 ,
# les algos proposent une precision a 1/16 pixel ;
# en cherchant les disparites avec un pas de 1/16
# les disparites reelless’ en retrouvent ∗16
maxdisp = 64
numdisp = maxdisp if maxdisp%16==0 else ((maxdisp//16)+1)*16
# Block Matching
# attention, cette implementationne fonctionne que
# pour des images en niveaux degris
stereoBM = cv.StereoBM.create(numDisparities=numdisp,\
                              blockSize=15) #blockSize impair
dispBM = stereoBM.compute(Iggray,Idgray).astype(np.float32) / 16.0
# Semi Global Block Matching
# peut traiter des images RGB ou en niveaux degris
stereoSGBM = cv.StereoSGBM.create(numDisparities=numdisp,\
blockSize=15) #impair
dispSGBM = stereoSGBM.compute(Ig,Id).astype(np.float32)/16.0
# StereoQuasiDense ,
# eparse puis densification par propagation
# necessite OpenCV >= 4 . 5
# travaille en niveau degris , meme avec des images RGB
# donne d i r e c t em e n t l e s d i s p a r i t e s r e l l e s
stereoQD = cv.stereo_QuasiDenseStereo.create(monoImgSize=Iggray.shape[::-1])
stereoQD.process(Iggray,Idgray)
dispQD = stereoQD.getDisparity()

#question 5
maskBM = np.zeros(dispBM.shape,np.float32)
maskBM[dispBM==-1]=1
maskSGBM = np.zeros(dispSGBM.shape,np.float32)
maskSGBM[dispSGBM==-1]=1
maskQD = np.zeros(dispSGBM.shape,np.float32)
maskQD[dispQD==np.nan]=1

#question 6
f = 994.978
b = 0.193001
dispBMD = np.zeros(dispBM.shape,np.float32)
dispBMD[maskBM==0] = np.log(f*b/dispBM[maskBM==0])
dispSGBMD = np.zeros(dispSGBM.shape,np.float32)
dispSGBMD[maskSGBM==0] = np.log(f*b/dispSGBM[maskSGBM==0])
dispQDD = np.zeros(dispSGBM.shape,np.float32)
dispQDD[maskQD==0] = np.log(f*b/dispQD[maskQD==0])

#question 7
# xx , yy = np.meshgrid(np.arange(Ig.shape[1]) ,np.arange(Ig.shape[0]))
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(xx[maskSGBM==0].flatten() , yy[maskSGBM==0].flatten(), \
# dispBMD[maskSGBM==0].flatten(),\
# s=1, c=Ig[maskSGBM==0].reshape((-1 ,3) )/ 255., \
# depthshade=False)
# ax.set_xlabel('X')
# ax.set_ylabel('Y')
# ax.set_zlabel('Z')
# ax.set_zlim([0.,5.] ) # l i m i t e r a i s o n n a b l e de z pour l a scene
# plt.show( )

#question 8
maskBMGT = np.ones(dispBM.shape,np.float32)
maskBMGT[gt==np.inf]=-1
maskBMGT[maskSGBM==1]=-2
for i in range(maskBMGT.shape[0]):
    for j in range(maskBMGT.shape[1]):
        if maskBMGT[i][j]==-2 and gt[i][j]==np.inf:
            maskBMGT[i][j]=2
#question 9
size=maskBMGT[maskBMGT==-1].size
#question 10-11
dispvBMD = np.zeros(dispBM.shape,np.float32)
dispvBMD[maskBMGT==1] = dispSGBM[maskBMGT==1]
dispvBMD[maskBMGT==1] = np.abs(dispvBMD[maskBMGT==1]-gt[maskBMGT==1])
x=dispvBMD[dispvBMD!=0].min()
y=dispvBMD[dispvBMD!=0].max()
axes[0][0].imshow(Ig)
axes[0][0].set_title(" Ig ")
axes[0][1].imshow(dispvBMD,cmap='gray',vmin=x,vmax=y)
axes[0][1].set_title(" Id ")
#question 12-13
d = np.zeros(dispBM.shape,np.float32)
d[maskBMGT!=-2] = dispvBMD[maskBMGT!=-2]
z=d[d>1].size+d[maskBMGT==-1].size+d[maskBMGT==2].size
io.imsave("./teddy/test.png",dispBM)
axes[1][0].imshow(dispQD,cmap='gray')
axes[1][0].set_title(" Ig ")
axes[1][1].imshow(gt,cmap='gray');
axes[1][1].set_title(" Id ")
plt.show()



