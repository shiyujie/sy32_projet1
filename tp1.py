#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 1 01:33:58 2022

@author: Yujie SHI and Ruimeng Wang
"""

import sys
import numpy as np
from skimage import io,exposure
from skimage.color import rgb2gray
from scipy import ndimage
import matplotlib.pyplot as plt
from scipy.linalg import norm
from matplotlib.colors import ListedColormap
#%matplotlib auto
if __name__ == '__main__':
    assert len(sys.argv) == 4, "Il faut 3 arguments : stereomatch.py im2.png im6.png disp2.png"
    Ig = io.imread(sys.argv[1])
    Id = io.imread(sys.argv[2])

    #ouvrir les images en niveaux de gris
    Iggray = (rgb2gray(Ig)*255).astype(np.float32)
    Idgray = (rgb2gray(Id)*255).astype(np.float32)


    fig,axes = plt.subplots(2,2,sharex=True,sharey=True)
    disp = np.zeros(Iggray.shape,np.float32)
    size = 75
    
    while size>2:
        disp=disp.astype(np.uint8)
        disp=disp.astype(np.float32)
        #Choisissez une fenêtre fixe 75*75 15*15 3*3
        for m in range(0,Idgray.shape[0],size):
            for n in range(0,Idgray.shape[1],size):
                #Calculer la valeur de décalage moyenne pour une fenêtre fixe
                dx=disp[m:m+size,n:n+size]
                mean_disp = np.mean(dx)
                
                #Obtenir le bon pixel correspondant à la prédiction
                i = max(0,min(n-mean_disp,Idgray.shape[1]-size))
                
                #Obtient les niveaux de gris moyens de la fenêtre
                window_left = Iggray[m:m+size,n:n+size]
                mt = np.mean(window_left)
                dis = size
                img=Idgray[m:m+size, int(i):int(i)+size]
                mi = np.mean(img)
                v = np.sum((img-mi) * (window_left-mt))
                v /= np.sqrt(np.sum((img-mi)**2)) * np.sqrt(np.sum((window_left-mt)**2))
                if(n-i<5):
                    r_max=min(n,n)
                    r_min=max(0,n-64)
                else:
                    r_max=min(n-int(mean_disp)+dis,n)
                    r_min=max(0,max(n-64,n-int(mean_disp)-dis))
                for x in range(r_min,r_max):
                    img=Idgray[m:m+size, x:x+size]
                    mi = np.mean(img)
                    _v = np.sum((img-mi) * (window_left-mt))
                    _v /= np.sqrt(np.sum((img-mi)**2)) * np.sqrt(np.sum((window_left-mt)**2))
                    if _v > v and n-x<64:
                        v = _v
                        i = x
                        disp[m:m+size,n:n+size]=n-i
        for m in range(size//2,Idgray.shape[0]-size//2-1,size):
            for n in range(size+size//2,Idgray.shape[1]-size//2-1,size):
                window_left = disp[m:m+size,n:n+size].copy()
                for i in range(size):
                    for j in range(size):
                        intre_a=(1.-j/size)*disp[m,n]+j/size*disp[m,n+size-1]
                        intre_b=(1.-j/size)*disp[m+size-1,n]+j/size*disp[m+size-1,n+size-1]
                        window_left[i,j]=(1.-i/size)*intre_a+i/size*intre_b
                disp[m:m+size,n:n+size]=window_left
        size//=5
    disp_t=disp.copy()
    disp_t*=4
    disp_t=disp_t.astype(np.uint8)
    disp_t[disp>255]=255

    axes[0][0].imshow(Ig[:,:])
    axes[0][0].set_title(" Ig ")
    axes[0][1].imshow(Id[:,:])
    axes[0][1].set_title(" Id ")

    io.imsave(sys.argv[3],disp_t)
    axes[1][0].imshow(disp_t,cmap='gray')
    axes[1][0].set_title(" Ig ")
    axes[1][1].imshow(Iggray,cmap='gray');
    axes[1][1].set_title(" Id ")
    plt.show()