#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu April 7 01:33:58 2022

@author: Yujie SHI and Ruimeng Wang
"""
import sys
import time
import numpy as np
from skimage import io,exposure
from skimage.color import rgb2gray
from scipy import ndimage
import matplotlib.pyplot as plt
from scipy.linalg import norm
from matplotlib.colors import ListedColormap
#%matplotlib auto
#ouvrirles images en RGB et en niveaux degris dans [ [ 0 ; 2 5 5 ] ]
if __name__ == '__main__':
    start = time.time()
    assert len(sys.argv) == 4, "Il faut 3 arguments : stereomatch.py im2.png im6.png estimate.png"
    Ig = io.imread(sys.argv[1])
    Id = io.imread(sys.argv[2])

    #ouvrir les images en niveaux de gris
    Iggray = (rgb2gray(Ig)*255).astype(np.float32)
    Idgray = (rgb2gray(Id)*255).astype(np.float32)


    fig,axes = plt.subplots(2,2,sharex=True,sharey=True)
    disp = np.zeros(Iggray.shape,np.float32)

    d_max=64
    cost = np.zeros((Iggray.shape[0],Iggray.shape[1],d_max+1),np.float32)
    cost.fill(-1)
    interval = np.zeros((Iggray.shape[0],Iggray.shape[1],2),np.uint8)
    size = 25
    m_size= 5
    for m in range(0,Idgray.shape[0],size):
        for n in range(0,Idgray.shape[1],size):
            dx=disp[m:m+size,n:n+size]
            md = np.mean(dx)
            i = max(0,min(n-md,Idgray.shape[1]-size))
            test = Iggray[m:m+size,n:n+size]
            mt = np.mean(test)
            dis = size
            img=Idgray[m:m+size, int(i):int(i)+size]
            mi = np.mean(img)
            v = np.sum((img-mi) * (test-mt))
            v /= np.sqrt(np.sum((img-mi)**2)) * np.sqrt(np.sum((test-mt)**2))
            if(n-i<5):
                r_max=min(n,n)
                r_min=max(0,n-64)
            else:
                r_max=min(n-int(md)+dis,n)
                r_min=max(0,max(n-64,n-int(md)-dis))
            interval[m:m+size,n:n+size,0]=n-r_max
            interval[m:m+size,n:n+size,1]=n-r_min
            for x in range(r_min,r_max):
                img=Idgray[m:m+size, x:x+size]
                mi = np.mean(img)
                _v = np.sum((img-mi) * (test-mt))
                _v /= np.sqrt(np.sum((img-mi)**2)) * np.sqrt(np.sum((test-mt)**2))
                cost[m+size//2,n+size//2,n-x]=_v
                if _v > v:
                    v = _v
                    i = x
                    disp[m:m+size,n:n+size]=n-i
    for m in range(size//2,Idgray.shape[0]-size//2-1,size):
        for n in range(size+size//2,Idgray.shape[1]-size//2-1,size):
            test = disp[m:m+size,n:n+size].copy()
            for i in range(size):
                for j in range(size):
                    intre_a=(1.-j/size)*disp[m,n]+j/size*disp[m,n+size-1]
                    intre_b=(1.-j/size)*disp[m+size-1,n]+j/size*disp[m+size-1,n+size-1]
                    test[i,j]=(1.-i/size)*intre_a+i/size*intre_b
            disp[m:m+size,n:n+size]=test
    size//=5
    disp=disp.astype(np.uint8)
    disp=disp.astype(np.float32)

    for m in range(0,Idgray.shape[0]-size,size):
        for n in range(0,Idgray.shape[1]-size,size):
            dx=disp[m:m+size,n:n+size]
            md = np.mean(dx)
            i = max(0,min(n-md,Idgray.shape[1]-size))
            test = Iggray[m:m+size,n:n+size]
            mt = np.mean(test)
            dis = size
            img=Idgray[m:m+size, int(i):int(i)+size]
            mi = np.mean(img)
            v = np.sum((img-mi) * (test-mt))
            v /= np.sqrt(np.sum((img-mi)**2)) * np.sqrt(np.sum((test-mt)**2))
            if(n-i<5 or n-i>60):
                r_max=min(n,n)
                r_min=max(0,n-64)
            else:
                r_max=min(n-int(md)+dis,n)
                r_min=max(0,max(n-64,n-int(md)-dis))
            interval[m:m+size,n:n+size,0]=n-r_max
            interval[m:m+size,n:n+size,1]=n-r_min
            for x in range(r_min,r_max):
                img=Idgray[m:m+size, x:x+size]
                mi = np.mean(img)
                _v = np.sum((img-mi) * (test-mt))
                _v /= np.sqrt(np.sum((img-mi)**2)) * np.sqrt(np.sum((test-mt)**2))
                cost[m+size//2,n+size//2,n-x]=_v
                if _v > v:
                    v = _v
                    i = x
                    disp[m:m+size,n:n+size]=n-i
    size=25
    cost_t = cost.copy()
    for m in range(size//2,Idgray.shape[0]-size//2,m_size):
        for n in range(size//2,Idgray.shape[1]-size//2,m_size):
            test_left  = Iggray[m-size//2:m+size//2+1,n-size//2:n+size//2+1]
            cost_block = cost[m-size//2:m+size//2+1,n-size//2:n+size//2+1]
            m_l=np.mean(Iggray[m-m_size//2:m+m_size//2+1,n-m_size//2:n+m_size//2+1])
            for k in range(d_max):
                if(cost[m,n,k]>0):
                    test_right  = Idgray[m-size//2:m+size//2+1,n-k-size//2:n-k+size//2+1]
                    m_r=np.mean(test_right[size//2-m_size//2:size//2+m_size//2+1,size//2-m_size//2:size//2+m_size//2+1])
                    c_up=0.
                    c_down=0.
                    for i in range(m_size//2,size-m_size//2,m_size):
                        for j in range(m_size//2,size-m_size//2,m_size):
                            dis=(i-size//2)**2+(j-size//2)**2
                            m_q=np.mean(test_left[i-m_size//2:i+m_size//2+1,j-m_size//2:j+m_size//2+1])
                            m_p=np.mean(test_right[i-m_size//2:i+m_size//2+1,j-m_size//2:j+m_size//2+1])
                            gris_g=(m_l-m_q)**2
                            gris_d=(m_r-m_p)**2
                            w_g=np.exp(-0.3*dis-0.6*gris_g)
                            w_d=np.exp(-0.3*dis-0.6*gris_d)
                            c_up+=w_g*w_d*cost_block[i,j,k]
                            c_down+=w_g*w_d    
                    if(c_down!=0):
                        cost_t[m,n,k]=c_up/c_down

    for m in range(size//2,Idgray.shape[0]-size//2-1,size):
        for n in range(size+size//2,Idgray.shape[1]-size//2-1,size):
            for k in range(d_max):
                test = cost_t[m:m+size,n:n+size].copy()
                for i in range(size):
                    for j in range(size):
                        intre_a=(1.-j/size)*cost_t[m,n]+j/size*cost_t[m,n+size-1]
                        intre_b=(1.-j/size)*cost_t[m+size-1,n]+j/size*cost_t[m+size-1,n+size-1]
                        test[i,j]=(1.-i/size)*intre_a+i/size*intre_b
                cost_t[m:m+size,n:n+size]=test
    size//=5
    for m in range(0,Idgray.shape[0]-size):
        for n in range(0,Idgray.shape[1]-size):
            v=-1
            for x in range(d_max):
                _v=cost[m+size//2,n+size//2,n-x]
                if _v > v:
                    v = _v
                    i = x
                    disp[m:m+size,n:n+size]=n-i
    disp*=4
    disp=disp.astype(np.uint8)
    disp[disp>255]=255

    axes[0][0].imshow(Ig[:,:])
    axes[0][0].set_title(" Ig ")
    axes[0][1].imshow(Id[:,:])
    axes[0][1].set_title(" Id ")
    io.imsave(sys.argv[3],disp)
    axes[1][0].imshow(disp,cmap='gray')
    axes[1][0].set_title(" Ig ")
    axes[1][1].imshow(Iggray,cmap='gray');
    axes[1][1].set_title(" Id ")
    plt.show()